module fft_mod
  use const_mod
  implicit none
contains
 
  recursive subroutine fft(x)
  complex(mp), dimension(:) :: x
  complex(mp) :: t
  integer :: N, i
  complex(mp), allocatable, dimension(:) :: even, odd

  N = size(x)
   
  if(N .le. 1) return
   
  allocate(odd((N+1)/2))
  allocate(even(N/2))
   
  odd=x(1:N:2)
  even=x(2:N:2)
   
  call fft(odd)
  call fft(even)

  do i=1,N/2
    t=exp(cmplx(0.0_mp,-2.0_mp*pi*real(i-1,mp)/real(N,mp),kind=mp))*even(i)
    x(i) = odd(i) + t
    x(i+N/2) = odd(i) - t
  enddo
   
  deallocate(odd)
  deallocate(even)
   
  end subroutine fft

  subroutine checkType(pm)

    character :: arg
    integer :: pm

    call getarg(1,arg)

    select case (arg)
          case ('a')
                pm = -1  !Прямое
          case ('b')
                pm = 1   !Обратное
          case default
                print*, 'Incorrect parameter for FFT type'
    end select

  end subroutine checkType

  subroutine PrepareData(x)

    complex(mp), dimension(:) :: x
    integer :: t

    call checkType(t)

    if (t .ge. 0) then
      x = conjg(x)
    endif

  end subroutine PrepareData
 
end module fft_mod