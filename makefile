plot: run
	gnuplot gr.plt
run: gen comp
	./a.out
comp: modcomp prog.f90
	gfortran prog.f90 *_mod.o -fopenmp
gen: gencomp
	./ser_gen
gencomp: modcomp ser_gen.f90
	gfortran ser_gen.f90 *_mod.o -fopenmp -o ser_gen
modcomp: *_mod.f90
	gfortran -c *_mod.f90