set terminal postscript eps enhanced size 7.0,2.0
set output "/mnt/d/OneDrive/Документы/res1_1.eps"
plot "D.dat" using 1:2 title 'Periodogram' with lines, "D.dat" u 1:3 title "Threshold" w l
set output "/mnt/d/OneDrive/Документы/notrend.eps"
plot "nl.dat" t "Series without linear trend" w l
set output "/mnt/d/OneDrive/Документы/series.eps"
plot "series.dat" t "Series" w l
set output "/mnt/d/OneDrive/Документы/corr.eps"
plot "corr.dat" u 1 t "Correlogram" w l
set output "/mnt/d/OneDrive/Документы/smResN0_1.eps"
plot "D.dat" u 1:4 t "Smoothed periodogram, N*=0.1N,a=0.25" w l
set terminal png interlace font arial 14 size 800,480 crop
set output "fftc.png"
plot "c.dat" u 2 w l, "c.dat" u 1 w l, "c.dat" u 3 w l